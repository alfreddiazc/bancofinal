<%-- 
    Document   : registrarCuenta
    Created on : 30/10/2019, 07:16:23 PM
    Author     : Usuario
--%>

<%@page import="com.sun.security.ntlm.Client"%>
<%@page import="Negocio.Banco"%>
<%@page import="DTO.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
        Banco banco=(Banco)(request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
            
%>
<html>
    <head>
        <title>Registro Cuenta</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <link rel="stylesheet" href="../../css/estilo.css">
        <link rel="stylesheet" href="../../css/select2.min.css"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"  crossorigin="anonymous">
        <link rel="icon" type="image/png" sizes="96x96" href="../../favicon/favicon-96x96.png">

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary  ">
            <a class="navbar-brand " href="../../index.html">Inicio</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../Cliente/registrar.jsp">Registrar nuevo cliente <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Informe/informe.jsp">Generar reportes</a>
                    </li>

                </ul>
                <div class="col-md-4 text-center">
                    <img class="" src="../../img/log.png">
                </div>
                <div class="col-md-4"></div>
            </div>
        </nav>
        <br>
        
        <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Registrar</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Eliminar</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        
        <div class="container ">
            <p class="bg-primary font-weight-bolder text-center">Por favor digitar los datos de la cuenta nueva:</p>
        </div>
            <form name="registrar" action="../../registrarcuenta.do" class="register">
            <p>Cedula:</p>
            <select id="cliente" name="cliente">
                <% 
//                    String [] c=banco.getClientes().split(",");
//                    
//                    for (String cl : c) {
//                        
                     %>    
                     
                     <option value=" ">Seleccione cedula o nombre</option>
                     
                     
                     <!--<option value="<% // = cl.indexOf(cl) %>"> <%//=  cl.toString() %></option>-->
                     <%
                         
//                        }
                %>
            </select>
            <button type="button" id="botoncito" onclick="llenar()">Cargar Clientes</button>
            <br>
            <br>
            <p>Numero de cuenta<input class="register-input" type="number" name="numero"  maxlength="10" id="numcuenta" value=""/></p> 
            <br>
            <p>Tipo de cuenta:</p>
            <input type="radio" name="tipo" value="true" checked="checked"> Cuenta de ahorros<br>
            <input type="radio" name="tipo" value="false"> Cuenta corriente<br>
            <input type="submit" value="registrar" name="registrar" class="btn btn-outline-primary btn-block" />
        </form>

    </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
     <form name="registrar" action="../../registrarcuenta.do" class="register">
           
      <p>Numero de cuenta:</p>
            <select id="cuentaOri" name="cuentaOri">
                <% //                    String[] cu = banco.getCuentas().split(",");

//                    for (String cl : cu) {

                %>    

                <option value="0">Seleccione numero cuenta</option>
                <!--<option value="<% // = cl.indexOf(cl)%>"> <% // = cl.toString()%></option>-->
                <%

//                    }
                %>
            </select>
             <input type="submit" value="eliminar" name="registrar" class="btn btn-outline-primary btn-block" />
       
     </form>
  </div>
</div>
        
        
        
        

           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../../js/select2.min.js"></script>
    <script>
        $("#cliente").select2( {
         placeholder: "Seleccione el cliente",
         allowClear: true
         });
    </script>
    <script>
    function llenar(){
        let dropdown = $('#cliente');
        dropdown.empty();

            dropdown.append('<option selected="true" disabled>Seleccione el cliente</option>');
            dropdown.prop('selectedIndex', 0);

            // Populate dropdown
                $.getJSON("../../json/clientes.json", function(result){
                  $.each(result, function (i,item) {
                    dropdown.append($('<option value='+ item.cedula +'>' + item.cedula+ " - " + item.nombre + '</option>'));
                  });
                });
    }
        </script>
         <script>
            function el(el) {
                return document.getElementById(el);
            }

            el('numcuenta').addEventListener('input', function () {
            var val = this.value;
                    this.value = val.replace(/\D|\-/, '');
            });
        </script>
         <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"  crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"  crossorigin="anonymous"></script>

    </body>
</html>

