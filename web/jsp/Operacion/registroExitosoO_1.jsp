<%-- 
    Document   : registroExitosoO
    Created on : 5/11/2019, 12:35:07 AM
    Author     : Usuario
--%>

<%@page import="Dto.Operacion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Negocio.Banco"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilo.css">
        <title>Registro exitoso</title>
    </head>
    <body>
        
        <%
        Banco banco=(Banco)(request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
            
        %>
        
        <h1 class="register-title">Registro de operacion exitoso</h1>
        <br>
        <hr>
        <% for (Operacion dato:banco.getOperaciones())
        {
        %>
            <p><%=dato.toString()%></p>
        <%
            }
        %>
        
        <hr>
        
        <a href="./jsp/Operacion/registrarOperacion.jsp">Registrar otra operacion</a>
    </body>
</html>
