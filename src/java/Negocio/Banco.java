/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import DAO.ClienteJpaController;
import DAO.Conexion;
import DAO.CuentaJpaController;
import DAO.TipoJpaController;
import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DTO.Cliente;
import DTO.Cuenta;
import DTO.Movimiento;
import DTO.Tipo;
import Util.Test;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class Banco {
    
    private Conexion con;
    
    public Banco() {
        con = Conexion.getConexion();
    }
    
    public boolean insertarCliente(int cedula, String nombre, String dir, String fecha, String email, int telefono) throws ParseException{
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Date fechan = dateFormat.parse(fecha);
 
        Cliente c = new Cliente(cedula, nombre, fechan, dir, telefono, email);
        
        try {
            ClienteJpaController cj = new ClienteJpaController(con.getBd());
            cj.create(c);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean actualizarCliente(Cliente cliente){
        ClienteJpaController cj = new ClienteJpaController(con.getBd());
        try {
            cj.edit(cliente);
            return true;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean eliminarCliente(int cliente){
        ClienteJpaController cj = new ClienteJpaController(con.getBd());
        try {
            cj.destroy(cliente);
            return true;
        } catch (IllegalOrphanException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean insertarCuenta(int nroCuenta, String fecha, int cedula, int tipo) throws ParseException{
        ClienteJpaController cjp  = new ClienteJpaController(con.getBd());
        TipoJpaController tjp = new TipoJpaController(con.getBd());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Date fechan = dateFormat.parse(fecha);
        Cliente cedulaAux = cjp.findCliente(cedula);
        Tipo tipoAux = tjp.findTipo(tipo);
        
        Cuenta c = new Cuenta(nroCuenta, 0, fechan);
        c.setCedula(cedulaAux);
        c.setTipo(tipoAux);
        
        try {
            CuentaJpaController cujp = new CuentaJpaController(con.getBd());
            cujp.create(c);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return false;
    }
    
    public boolean editarCuenta(Cuenta cuenta){
        CuentaJpaController cujp = new CuentaJpaController(con.getBd());
        try {
            cujp.edit(cuenta);
            return true;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean eliminarCuenta(int nroCuenta){
        CuentaJpaController cujp = new CuentaJpaController(con.getBd());
        try {
            cujp.destroy(nroCuenta);
            return true;
        } catch (IllegalOrphanException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean registrarTransferencia(){
        return false;
    }
    
    public boolean registrarMovimiento(int id, String fecha, int valor, int cuenta, int tipo){
        ClienteJpaController cjp = new ClienteJpaController(con.getBd());
        CuentaJpaController cujp = new CuentaJpaController(con.getBd());
        
        
    return false;
    }
    
    public String extractoCliente(int cedula, String fechaInicio, String fechaFin) throws ParseException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaI = dateFormat.parse(fechaInicio);
        Date fechaF = dateFormat.parse(fechaFin);
        ClienteJpaController cjp  = new ClienteJpaController(con.getBd());
        Cliente cedulaAux = cjp.findCliente(cedula);
        String aux = "{cedula:\"" + cedulaAux.getCedula() + "\", cuentas:[";
        List<Cuenta> cuentas = cedulaAux.getCuentaList();
        int iaux = 0;
        for (Cuenta c : cuentas) {
            String aux2 = "{numero:\"" + c.getNroCuenta() + "\", tipo:\"" + c.getTipo().getNombre() + "\", movimientos:[";
            List<Movimiento> mov = c.getMovimientoList();
            for (Movimiento m : mov) {
                Date fm = m.getFecha();
                if (fm.after(fechaI) && fm.before(fechaF)) {
                        iaux++;
                        aux2 += "{id:\"" + m.getId() + "\", tipo:\"" + m.getIdTipoMovimiento().getDescripcion()+
                            "\", monto:\"" + m.getValor() + "\"},";
                }
            }
            aux2 = aux2.substring(0,aux2.length()-1);
            
            if (iaux > 0) {
                aux += aux2 + "]},"; 
            }else{
                aux += "\"No hay Movimientos\",";
            }
        }
        aux = aux.substring(0,aux.length()-1);
        aux += "]}";
        return aux;
    }
    
    public String getClientes(){
        
        String str="alf,juan,jose";
        return str;
    }
    public String getCuentas(){
        String str="alf,juan,jose";
        return str;
    }
}
