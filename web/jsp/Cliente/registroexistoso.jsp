<%-- 
    Document   : registroexistoso
    Created on : 17/10/2019, 09:19:07 AM
    Author     : docente
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="Negocio.Banco"%>
<%@page import="DAO.ClienteJpaController"%>
<%@page import="DTO.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilo.css">
        <title>Registro exitoso</title>
    </head>
    <body>
        
        <div class="container">
             <nav class="navbar navbar-expand-lg navbar-dark bg-primary  ">
            <a class="navbar-brand " href="#">Inicio</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="./jsp/Cliente/registrar.jsp">Nuevo cliente<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contacto">Generar reportes</a>
                    </li>

                </ul>
                <div class="col-md-4 text-center">
                    <img class="" src="../../img/log.png">
                </div>
                <div class="col-md-4"></div>
            </div>
        </nav>
        </div>
        <div class="container">
            <table class="table">
            <thead class="thead-light">
                <tr>
                    <th></th>
                    <th>Cedula</th>
                    <th>Nombre</th>
                    <th>E-mail</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var ="datos" items="${datos}">
                    <tr>
                        <th scope="row">${dato.getCedula()}</th>
                        <td>${dato.getNombre()}</td>
                        <td>${dato.getEmail()}</td>
                        <td>${dato.getDireccion}</td>
                        <td>${dato.getTelefono}</td>
                        <td>${dato.getFecha}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        </div>

        
    </body>
</html>
