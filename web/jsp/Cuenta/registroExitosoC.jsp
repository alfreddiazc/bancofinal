<%-- 
    Document   : registroExitosoC
    Created on : 30/10/2019, 09:32:24 PM
    Author     : Usuario
--%>

<%@page import="DTO.Cuenta"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Negocio.Banco"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilo.css">
        <title>Registro exitoso</title>
    </head>
    <body>
        
        <%
        Banco banco=(Banco)(request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
            
        %>
        
        <h1 class="register-title">Registro de cuenta exitoso</h1>
        <br>
        <a href="./jsp/Cuenta/registrarCuenta.jsp">Registrar otra cuenta</a>
    </body>
</html>
