<%-- 
    Document   : informe
    Created on : 5/11/2019, 01:09:56 AM
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Negocio.Banco"%>
<%
    Banco banco = (Banco) (request.getSession().getAttribute("banco"));
    request.getSession().setAttribute("banco", banco);

%>

<html>
    <head>
        <title>Realizar informes</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../css/estilo.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"  crossorigin="anonymous">
        <link rel="stylesheet" href="../../css/select2.min.css"> 
        <link rel="icon" type="image/png" sizes="96x96" href="../../favicon/favicon-96x96.png">




    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary  ">
            <a class="navbar-brand " href="../../index.html">Inicio</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../Cliente/registrar.jsp">Registrar nuevo cliente <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Cuenta/registrarCuenta.jsp">Registrar una cuenta <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Operacion/registrarOperacion.jsp">Realizar nueva operación</a>
                    </li>

                </ul>
                <div class="col-md-2 text-center">
                    <img class="" src="../../img/log.png">
                </div>
                <div class="col-md-2"></div>
            </div>
        </nav>
        <br>
        <div class="container ">
            <p class="bg-dark text-white font-weight-bolder text-center">Selecciones el informe que desea ver:</p>
        </div>
        <div id="accordion" role="tablist">
            <div class="card">
                <div class="card-header" role="tab" id="headingOne">
                    <h5 class="mb-0">
                        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="font-weight-bold text-dark">
                            Generar informe por cliente.
                        </a>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" style="">
                    <div class="card-body">
                        <form name="registrar" action="../../registrar.do" class="register">
                            <p>Cedula:</p>
                            <select id="cliente" name="cliente">
                                <% //     String[] c = banco.getClientes().split(",");

//                                    for (String cl : c) {

                                %>    

                                <option value="0">Seleccione cedula o nombre</option>
                                <option value="<% // = cl.indexOf(cl)%>"> <% // = cl.toString()%></option>
                                <%

//                                    }
                                %> </select>
                            <button type="button" id="botoncito" onclick="llenar()">Cargar Clientes</button>
                            <p>Fecha Inicio:<input class="register-input"  type="date" name="fechainicio" value="" /></p>
                            <p>Fecha Fin:<input class="register-input"  type="date" name="fechafin" value="" /></p>
                            <input type="submit" value="registrar" name="registrar" class="btn btn-outline-primary btn-block" />
                        </form>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="headingTwo">
                    <h5 class="mb-0">
                        <a class="font-weight-bold text-dark" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Generar informe por cuenta
                        </a>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" style="">
                    <div class="card-body">
                        <form name="registrar" action="../../registrar.do" class="register">
                            <p>Numero de cuenta:</p>
                            <select id="cuentaOri" name="cuentaOri">
                                <%
//                                    String[] cu = banco.getCuentas().split(",");
//
//                                    for (String cl : cu) {

                                %>    

                                <option value="0">Seleccione cedula o nombre</option>
                                <option value="<% // = // cl.indexOf(cl)%>"> <% // = // cl.toString()%></option>
                                <%

//                                    }
                                %>
                            </select>
                            <button type="button" id="botoncito" class="btn btn-outline-primary " onclick="llenar()">Cargar Cuentas</button>
                            <p>Fecha Inicio:<input class="register-input"  type="date" name="fechainicio" value="" /></p>
                            <p>Fecha Fin:<input class="register-input"  type="date" name="fechafin" value="" /></p>
                            <input type="submit" value="registrar" name="registrar" class="btn btn-outline-primary btn-block" />
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!--        <form name="registrar" action="../../registraroperacion.do" class="register">
                    
                    <button class="myButton" type="button" id="botoncito1" onclick="mostrar1()">Generar informe por clientes</button>
                    <br>
                    <br>
                    <p id="ced" style="display: none;">Cedula del cliente:</p>
                    <select id="cliente" name="cliente" style=" display: none;">
                        <option value="">Seleccione identificacion del cliente</option>
                    </select>
                    <input type="submit" value="registrar" id="reg1" name="registrar" class="register-button" style=" display: none;"/>
                    <br>
                    <br>
                    
                    <button class="myButton" type="button" id="botoncito2" onclick="mostrar2()">Generar informe por cuentas</button>
                    <br>
                    <br>
                    <p id="cue" style="display: none;">Numero de cuenta:</p>
                    <select id="cuenta" name="cuenta" style=" display: none;">
                        <option value="">Seleccione numero de cuenta</option>
                    </select>
                    <input type="submit" value="registrar" id="reg2" name="registrar" class="register-button" style=" display: none;"/>
                    <br>
                    <br>
                    
                    
                    
                    
                </form>-->
        <!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script src="../../js/select2.min.js"></script>
                <script>
                                        $("#cliente").select2({
                                            placeholder: "Seleccione el cliente",
                                            allowClear: true
                                        });
                                        $('#cliente').next(".select2-container").hide();
        
                                        $("#cuenta").select2({
                                            placeholder: "Seleccione la cuenta",
                                            allowClear: true
                                        });
                                        $('#cuenta').next(".select2-container").hide();
        
                                        function mostrar1() {
                                            $('#cliente').next(".select2-container").show();
                                            document.getElementById("ced").style.display = 'block';
                                            document.getElementById("reg1").style.display = 'block';
                                            $('#cuenta').next(".select2-container").hide();
                                            document.getElementById("cue").style.display = 'none';
                                            document.getElementById("reg2").style.display = 'none';
        
                                            let dropdown = $('#cliente');
                                            dropdown.empty();
                                            dropdown.append('<option selected="true" disabled>Seleccione el cliente</option>');
                                            dropdown.prop('selectedIndex', 0);
        
                                            // Populate dropdown
                                            $.getJSON("../../json/clientes.json", function (result) {
                                                $.each(result, function (i, item) {
                                                    dropdown.append($('<option value=' + item.cedula + '>' + item.cedula + " - " + item.nombre + '</option>'));
                                                });
                                            });
                                        }
        
                                        function mostrar2() {
                                            $('#cliente').next(".select2-container").hide();
                                            document.getElementById("ced").style.display = 'none';
                                            document.getElementById("reg1").style.display = 'none';
                                            $('#cuenta').next(".select2-container").show();
                                            document.getElementById("cue").style.display = 'block';
                                            document.getElementById("reg2").style.display = 'block';
        
                                            let dropdown = $('#cuenta');
                                            dropdown.empty();
                                            dropdown.append('<option selected="true" disabled>Seleccione la cuenta</option>');
                                            dropdown.prop('selectedIndex', 0);
        
                                            // Populate dropdown
                                            $.getJSON("../../json/cuentas.json", function (result) {
                                                $.each(result, function (i, item) {
                                                    dropdown.append($('<option value=' + item.nroCuenta + '>' + item.nroCuenta + " - " + item.cliente.cedula + '</option>'));
                                                });
                                            });
                                        }
                </script>-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="../../js/select2.min.js"></script>
        <script>
                                $("#cliente").select2({
                                    placeholder: "Seleccione el cliente",
                                    allowClear: true
                                });
        </script>
        <script>
            function llenar() {
                let dropdown = $('#cliente');
                dropdown.empty();

                dropdown.append('<option selected="true" disabled>Seleccione el cliente</option>');
                dropdown.prop('selectedIndex', 0);

                // Populate dropdown
                $.getJSON("../../json/clientes.json", function (result) {
                    $.each(result, function (i, item) {
                        dropdown.append($('<option value=' + item.cedula + '>' + item.cedula + " - " + item.nombre + '</option>'));
                    });
                });
            }
        </script>
        <script>

            $("#cuentaOri").select2({
                placeholder: "Seleccione la cuenta",
                allowClear: true
            });
            function llenar() {
                let dropdown = $('#cuentaOri');
                dropdown.empty();
                dropdown.append('<option selected="true" disabled>Seleccione la cuenta</option>');
                dropdown.prop('selectedIndex', 0);

                // Populate dropdown
                $.getJSON("../../json/cuentas.json", function (result) {
                    $.each(result, function (i, item) {
                        dropdown.append($('<option value=' + item.nroCuenta + '>' + item.nroCuenta + " - " + item.cliente.cedula + '</option>'));
                    });
                });
            }
        </script>
        <script>

            function el(el) {
                return document.getElementById(el);
            }

            el('cedula').addEventListener('input', function () {
                var val = this.value;
                this.value = val.replace(/\D|\-/, '');
            });
        </script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"  crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"  crossorigin="anonymous"></script>

    </body>
</html>

